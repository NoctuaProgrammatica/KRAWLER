#ifndef KORIENTATION_H
#define KORIENTATION_H

#include <SF2D/SF2D.h>

namespace sf2d
{
	//2x2 rotation matrix for orientation collision detection
	//m00,m01 = cos(theta), -sin(theta)
	//m10,m11 = sin(theta), cos(theta) 
	// (Transposing of this orientation will provide the inverse
	//	rotation)
	struct OrientationData
	{
		union
		{
			struct
			{
				float m00, m01;
				float m10, m11;
			};

			float m[2][2];
			float v[4];
		};

		SF2D_API OrientationData();
		SF2D_API OrientationData(float angleInDeg);
		SF2D_API OrientationData(float mat00, float mat01, float mat10, float mat11);

		SF2D_API void setOrientation(float angleInDeg);
		SF2D_API OrientationData getAbsoluteOrientation() const;

		SF2D_API Vec2f getXAxis() const;
		SF2D_API Vec2f getYAxis() const;

		SF2D_API OrientationData getTransposedOrientation() const;
		SF2D_API Vec2f operator*(const Vec2f& rhs) const;
		SF2D_API OrientationData operator* (const OrientationData& rhs) const;
	};
}

#endif