#pragma once 

#include <vector>
#include <memory>
#include <functional>

#include <SF2D/SF2D.h>
#include <SF2D/Components/ColliderBase.h>

#include <unordered_set>
#include <memory>

class b2DynamicTree;
struct b2AABB;
class b2BroadPhase;
struct b2RayCastInput;

namespace sf2d
{
	namespace collisions
	{
		/* Engine internal use only class.
		Won't be exposed through DLL. */

		struct RaycastCallbackData
		{
			Entity* pEntity = nullptr;
			Vec2f normal;
			float fraction = 0.0f;
			comps::ColliderBase* pCollider;
		};

		using RaycastCallback = std::function<bool(const RaycastCallbackData&)>;

		class CollisionOverlord
		{
		public:

			CollisionOverlord();
			~CollisionOverlord();

			// Triggered when onExitScene() is called to clean up the tree 
			void triggerSceneCleanup();

			// Triggered after onEnterScene() to construct the tree
			void triggerSceneConstruct();

			// Update proxy positions on tick
			void tick();

			SF2D_API void castRayInScene(const Vec2f& start, const Vec2f& end, const RaycastCallback& cb, Entity* pCastingEntity = nullptr);

			// Does the tag passed match as the tag on the proxy id given
			bool doesTagMatchProxyId(const std::string& tag, int id) const;

		private:
			using ProxyPair = std::pair<int32, int32>;
			struct ProxyInfo
			{
				int32 proxyId;
				comps::ColliderBase* pCollider;
				b2AABB* aabb;
				Vec2f lastPos;
				Entity* pEntity;
				std::vector<ProxyPair>* pToCheck;

				bool QueryCallback(int neighbourProxyId)
				{
					if (neighbourProxyId == proxyId)
					{
						// we've stumbled upon ourselves
						return true;
					}

					//possibleIntersections.push(neighbourProxyId);
					pToCheck->push_back(ProxyPair(proxyId, neighbourProxyId));
					return true;
				}
			};

			struct RaycastCBInternal
			{
				CollisionOverlord* pOverlord = nullptr;
				std::vector<ProxyInfo>* pProxies;
				int castingID = -1;
				float RayCastCallback(const b2RayCastInput& input, int id);
				//bool bDidHit = false;
				const RaycastCallback* externalCallback;
			};

			void cleanupProxies();

			// Handles the movement of proxies withing the dynamic trees
			void relocateProxies();

			// will force query calls for each entity
			void checkForProxyInteractions();

			// will perform in-depth narrow phase by using collider 
			void performNarrowPhaseForProxies();

			void generateNarrowPhaseQueue();

			int32 getProxyIndexFromId(int32 proxyId) const;

			std::unique_ptr<b2BroadPhase> m_pBroadPhase;

			std::vector<ProxyInfo> m_proxies;
			std::vector<ProxyPair> m_intersectionsToCheck;
			std::deque<ProxyPair> m_narrowPhaseQueue;

			void circleCircle(const ProxyInfo& proxyA, const b2Transform& transformA, const ProxyInfo proxyB, const b2Transform& transformB, comps::CollisionDetectionData& data);
			void polyCircle(const ProxyInfo& proxyA, const b2Transform& transformA, const ProxyInfo proxyB, const b2Transform& transformB, comps::CollisionDetectionData& data);
			void circlePoly(const ProxyInfo& proxyA, const b2Transform& transformA, const ProxyInfo proxyB, const b2Transform& transformB, comps::CollisionDetectionData& data);
			void polyPoly(const ProxyInfo& proxyA, const b2Transform& transformA, const ProxyInfo proxyB, const b2Transform& transformB, comps::CollisionDetectionData& data);

			void test(int) {}


			std::function<void(const ProxyInfo&, const b2Transform&, const ProxyInfo, const b2Transform&, comps::CollisionDetectionData&)>m_collisionLookup[2][2];
			/*	void(KCollisionOverlord::* m_collisionLookup[2][2])(const ProxyInfo&, const b2Transform&, const ProxyInfo, const b2Transform&, KCollisionDetectionData&) =
				{
				};*/


		};
	}
}
