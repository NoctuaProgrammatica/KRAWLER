#pragma once

#include <SF2D/SF2DLib.h>
#include <SF2D/SF2D.h>
#include <SF2D/Scene.h>
#include <string>

namespace sf2d
{
	class SystemBase
	{
	public:
		SF2D_API SystemBase();
		SF2D_API SystemBase(std::string_view id);
		SF2D_API SystemBase(std::string_view id, sf2d::Scene& scene);

		SF2D_API virtual ~SystemBase() = default;
		SF2D_API SystemBase(const SystemBase& base) = default;
		SF2D_API SystemBase(SystemBase&& base) = default;

		SF2D_API [[nodiscard]] virtual InitStatus init() = 0;
		SF2D_API virtual void onEnterScene() = 0;
		SF2D_API virtual void onExitScene() = 0;
		SF2D_API virtual void tick() = 0;
		SF2D_API virtual void cleanup() = 0;

		SF2D_API [[maybe_unused]] std::string_view getID() const;

		SF2D_API [[nodiscard]] SystemBase& operator= (const SystemBase& base) = default;
		SF2D_API [[nodiscard]] SystemBase& operator= (SystemBase&& base) = default;

	protected:
		sf2d::Scene& m_currentScene;

	private:

		std::string m_id;

	};
}