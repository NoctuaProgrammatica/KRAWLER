#pragma once 

#include <SF2D/SF2DLib.h>

#include <SF2D/Utilities/EngineMacros.h>
#include <SF2D/Maths/Maths.hpp>

#include <SF2D/Utilities/EngineMacros.h>
#include <SF2D/Utilities/DebugTools.h>
#include <SFML/Graphics/Color.hpp>

#include <map>

#include <SFML/Graphics/Rect.hpp>
#include <SFML/System/Vector3.hpp>
#include <spdlog/spdlog.h>

namespace sf2d
{
	using int8 = signed char;
	using uint8 = unsigned char;

	using int16 = signed short;
	using uint16 = unsigned short;

	using int32 = signed int;
	using uint32 = unsigned int;

	using int64 = signed   __int64;
	using uint64 = unsigned __int64;

	using Vec2f = sf::Vector2f;
	using Vec2i = sf::Vector2i;
	using Vec2u = sf::Vector2u;
	using Vec2d = sf::Vector2<double>;

	using Vec3f = sf::Vector3f;

	using Point = sf::Vector2f;
	using Colour = sf::Color;

	using Rectf = sf::Rect<float>;
	using Rectd = sf::Rect<double>;
	using Recti = sf::Rect<int32>;
	using Rectu = sf::Rect<uint32>;


	struct AppInit;

	enum class InitStatus : int32
	{
		Success = 0,
		Failure = -1,
		Nullptr = -2,
		MissingResource = -3
	};
#define INIT_CHECK(func)																															\
			{\
	sf2d::InitStatus status = func;																										\
		if (status != sf2d::InitStatus::Success)																								\
		{																																		\
			std::cout << "Failed to initialise" << __FUNCSIG__ << " in " << __FILE__ << " at line " << __LINE__ << " due to ";								\
			switch (status)																														\
			{																																	\
			case sf2d::InitStatus::Failure: std::cout << "General Failure!" << std::endl; break;										\
			case sf2d::InitStatus::Nullptr: std::cout << "Null pointer!" << std::endl; break;											\
			case sf2d::InitStatus::MissingResource: std::cout << "Missing Resource!" << std::endl; break;								\
			}																																	\
			if(status!= sf2d::InitStatus::Success) { return status;}\
		}\
	}
	SF2D_API [[nodiscard]] InitStatus StartupEngine(AppInit* windowInit);
	SF2D_API [[nodiscard]] InitStatus InitialiseSubmodules();

	SF2D_API void ShutdownEngine();
	SF2D_API void RunApplication();

	SF2D_API std::string GenerateUUID();

};

