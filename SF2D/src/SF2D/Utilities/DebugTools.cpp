#include <SF2D/Utilities/DebugTools.h>
#include <fstream> //for wofstream

#ifdef _WIN32
#include <Windows.h>
#endif 

#include <stdio.h>	//for sprintf
#include <mutex>	
#include <sstream>
#include <iomanip>
#include <SFML/System/String.hpp>
#include <chrono>

#include <spdlog/spdlog.h>

inline std::chrono::high_resolution_clock::time_point sf2d::profile::StartFunctionTimer()
{
	return std::chrono::high_resolution_clock::now();
}

inline long long sf2d::profile::EndFunctionTimer(const std::chrono::high_resolution_clock::time_point& t1, const std::string& funcName, bool bIsMicroseconds, bool bLogFileInsteadOfConsole)
{
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	long long duration;
	if (bIsMicroseconds)
	{
		duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	}
	else
	{
		duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
	}


	spdlog::critical("{} execution time: {}  {}\n", funcName, duration, bIsMicroseconds ? "Microseconds" : "Milliseconds");
	return duration;
}
