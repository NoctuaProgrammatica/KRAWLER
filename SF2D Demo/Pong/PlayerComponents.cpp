#include "stdafx.h"
#include "PlayerComponents.h"

void PlayerPaddleController::reset()
{
	sf2d::Vec2f pos;
	pos.x = (PADDLE_SIZE.x / 2.0f + 10.0f);
	pos.y = static_cast<float>(GET_APP()->getRenderWindow()->getSize().y) / 2.0f;
	m_entity->transform->setPosition(pos);
}

void PlayerPaddleController::collisionCb(const sf2d::comps::CollisionDetectionData& data)
{
}

void PlayerPaddleController::onEnterScene()
{
	auto spriteComp = m_entity->getComponent<sf2d::comps::Sprite>();
	assert(spriteComp);
	spriteComp->setSize(PADDLE_SIZE);
	spriteComp->setColour(sf2d::Colour::Yellow);

	auto coll = m_entity->getComponent<sf2d::comps::ColliderBase>();
	coll->subscribeCollisionCallback(&m_cbFunc);

	reset();
}

void PlayerPaddleController::tick()
{
	const auto dt = GET_APP()->getDeltaTime();
	if (sf2d::input::InputHandler::Pressed(sf::Keyboard::W))
	{
		m_entity->transform->move(sf2d::Vec2f(0.0f, -1.0f) * dt * MOVE_SPEED);
	}

	if (sf2d::input::InputHandler::Pressed(sf::Keyboard::S))
	{
		m_entity->transform->move(sf2d::Vec2f(0.0f, 1.0f) * dt * MOVE_SPEED);
	}

	auto currentPos = sf2d::Vec2f{ m_entity->transform->getPosition() };
	const auto WindowSize = GET_APP()->getWindowSize();

	const float MinY = currentPos.y - (PADDLE_SIZE.y / 2.0f);
	const float MaxY = currentPos.y + (PADDLE_SIZE.y / 2.0f);

	if (MinY < 0.0f)
	{
		currentPos.y = PADDLE_SIZE.y / 2.0f;
	}
	else if (MaxY > WindowSize.y)
	{
		currentPos.y = WindowSize.y - (PADDLE_SIZE.y / 2.0f);
	}

	m_entity->transform->setPosition(currentPos);
}
