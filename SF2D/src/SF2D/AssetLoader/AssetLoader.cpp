#include <SF2D/AssetLoader/AssetLoader.h>
#include <SF2D/Animation/Animation.h>

#include <future>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <filesystem>
#include <sstream>

#include <rapidxml/rapidxml.hpp>
#include <string.h>
#include <nlohmann/json.hpp>

using namespace sf;
using namespace sf2d;
using namespace std;
using namespace rapidxml;

using json = nlohmann::json;

constexpr int32 MAX_ANIMATION_FILE_CHARS = 100000;

// HELPER FUNCTIONS

std::string FindFilename(std::string &str)
{
	std::string returnStr;
	uint32 idx = static_cast<unsigned>(str.size() - 1);
	bool bIsFilename = false;
	while (idx != 0)
	{
		if (!bIsFilename)
		{
			if (str[idx] == '.')
			{
				bIsFilename = true;
			}
		}
		else
		{
			if (str[idx] != '\\')
			{
				returnStr.push_back(str[idx]);
			}
			else
			{
				break;
			}
		}
		--idx;
	}

	return std::string(returnStr.rbegin(), returnStr.rend());
}

// ASSET LOADER
AssetLoader::~AssetLoader()
{
}

void AssetLoader::cleanupAssetLoader()
{
	for (auto &pair : m_texturesMap)
	{
		KFREE(pair.second);
	}
	m_texturesMap.clear();

	for (auto &pair : m_soundBufferMap)
	{
		KFREE(pair.second);
	}
	m_soundBufferMap.clear();

	for (auto &pair : m_fontMap)
	{
		KFREE(pair.second);
	}
	m_fontMap.clear();

	for (auto &pair : m_animationsMap)
	{
		KFREE(pair.second);
	}
	m_animationsMap.clear();

	for (auto &pair : m_importedLevelsMap)
	{
		tiled::cleanupLevelMap(pair.second);
		pair.second = nullptr;
	}

	for (auto &pair : m_musicFilesMap)
	{
		KFREE(pair.second);
	}
	m_musicFilesMap.clear();

	m_importedLevelsMap.clear();
}

sf::Texture *const AssetLoader::getTexture(const std::string &name)
{
	auto findResult = m_texturesMap.find(name);

	if (findResult == m_texturesMap.end())
	{
		spdlog::error("Failed to find texture with name {}", name);
		return m_texturesMap["missing"];
	}

	return findResult->second;
}

sf::SoundBuffer *const AssetLoader::getSound(const std::string &name)
{
	auto findResult = m_soundBufferMap.find(name);
	if (findResult == m_soundBufferMap.end())
	{
		spdlog::error("Failed to find sound with name {}", name);
		return nullptr;
	}

	return findResult->second;
}

sf::Shader *const AssetLoader::getShader(const std::string &name)
{
	auto findResult = m_shaderMap.find(name);
	if (findResult == m_shaderMap.end())
	{
		spdlog::error("Failed to find shader with name {}", name);
		return nullptr;
	}

	return findResult->second;
}

sf::Font *const AssetLoader::getFont(const std::string &name)
{
	auto findResult = m_fontMap.find(name);
	if (findResult == m_fontMap.end())
	{
		spdlog::error("Failed to font with name {}", name);
		return nullptr;
	}

	return findResult->second;
}

sf::Music *const AssetLoader::getMusic(const std::string &name)
{
	auto findResult = m_musicFilesMap.find(name);
	if (findResult == m_musicFilesMap.end())
	{
		spdlog::error("Failed to music with name {}", name);
		return nullptr;
	}

	return findResult->second;
}

anim::AnimationDef *const AssetLoader::getAnimation(const std::string &name)
{
	auto findResult = m_animationsMap.find(name);
	if (findResult == m_animationsMap.end())
	{
		spdlog::error("Failed to find animation with name {}", name);
		return nullptr;
	}

	return findResult->second;
}

tiled::MapRoot *const AssetLoader::getLevelMap(const std::string &name)
{
	auto findResult = m_importedLevelsMap.find(name);
	if (findResult == m_importedLevelsMap.end())
	{
		spdlog::error("Failed to find level map with name {}", name);
		return nullptr;
	}

	return findResult->second;
}

void AssetLoader::loadTexture(const std::string &name, const std::string &filePath)
{
	Texture *pTex = new Texture;

	if (m_texturesMap.find(name) != m_texturesMap.end())
	{
		spdlog::error("Already loaded {} - skipping!", filePath);
		return;
	}

	if (!pTex->loadFromFile(filePath))
	{
		KFREE(pTex);
		return;
	}

	if (pTex->getSize().x > 4096 || pTex->getSize().y > 4096)
	{
		spdlog::error("Asset load error: Texture {} is larger in dimensions than 4096 px!", name);
	}

	pTex->generateMipmap();
	m_texturesMap.emplace(name, pTex);
}

void AssetLoader::loadSound(const std::string &name, const std::string &filePath)
{
	if (m_soundBufferMap.find(name) != m_soundBufferMap.end())
	{
		spdlog::error("Already loaded {} - skipping!", filePath);
		return;
	}

	SoundBuffer *pSoundBuffer = new SoundBuffer;
	KCHECK(pSoundBuffer);
	sf::String s(filePath);

	if (!pSoundBuffer->loadFromFile(s.toAnsiString()))
	{
		delete pSoundBuffer;
		spdlog::error("Failed to load sound {}", name);
		return;
	}

	m_soundBufferMap.emplace(name, pSoundBuffer);
}

void AssetLoader::loadFont(const std::string &name, const std::string &filePath)
{
	if (m_fontMap.find(name) != m_fontMap.end())
	{
		spdlog::error("Already loaded {} - skipping!", filePath);
		return;
	}

	Font f;
	const sf::String s(filePath);

	if (!f.loadFromFile(s.toAnsiString()))
	{
		spdlog::error("Failed to load font {}", name);
		return;
	}

	m_fontMap.emplace(name, new Font(f));
}

void AssetLoader::loadTilemap(const std::string &name, const std::string &filePath)
{
	if (m_importedLevelsMap.find(name) != m_importedLevelsMap.end())
	{
		spdlog::error("Already loaded {} - skipping!", filePath.c_str());
		return;
	}

	tiled::MapRoot *pMap;
	pMap = tiled::loadTiledJSONFile(filePath);
	KCHECK(pMap);
	if (!pMap)
	{
		return;
	}
	m_importedLevelsMap.emplace(name, pMap);
}

void AssetLoader::loadMusic(const std::string &name, const std::string &filePath)
{
	if (m_musicFilesMap.find(name) != m_musicFilesMap.end())
	{
		spdlog::error("Already loaded {} - skipping!", filePath);
		return;
	}

	sf::Music *pMusicFile = new sf::Music();
	KCHECK(pMusicFile);
	sf::String s(filePath);

	if (!pMusicFile->openFromFile(s.toAnsiString()))
	{
		delete pMusicFile;
		spdlog::error("Failed to load music file {}", name);
		return;
	}

	m_musicFilesMap.emplace(name, pMusicFile);
}

void AssetLoader::loadShader(const std::string &shaderName, const std::string &vertPath, const std::string fragPath)
{
	if (m_shaderMap.find(shaderName) != m_shaderMap.end())
	{
		spdlog::error("Already loaded {} - skipping!", shaderName);
		return;
	}

	Shader *const pShader = new Shader();
	KCHECK(pShader);

	if (!pShader->loadFromFile(vertPath, fragPath))
	{
		spdlog::error("Failed shader name: {}", shaderName);
		delete pShader;
		return;
	}

	m_shaderMap.emplace(shaderName, pShader);
}

AssetLoader::AssetLoader() : m_rootFolder("res\\")
{
	auto path = m_rootFolder + "engine\\missing.png";
	loadTexture("missing", path);
	//std::thread thread_a(&KAssetLoader::scanFolderLoad, this);
	std::thread thread_a(&AssetLoader::loadAssetXML, this);
	std::thread thread_b(&AssetLoader::loadAnimationsXML, this);
	thread_a.join();
	thread_b.join();
	matchAnimationsToTextures();
}

void sf2d::AssetLoader::loadAssetXML()
{
	std::stringstream buffer;
	std::ifstream xmlFile;

	const auto path = m_rootFolder + XML_FILE;
	xmlFile.open(path);
	if (xmlFile.fail())
	{
		spdlog::error("Unable to find {}", path);
		return;
	}

	buffer << xmlFile.rdbuf();
	xmlFile.close();

	rapidxml::xml_document<> doc;
	auto buffStr = buffer.str();
	doc.parse<0>(&buffStr[0]);

	auto parentNode = doc.first_node("assets");

	// Textures
	auto texturesNode = parentNode->first_node("textures");
	if (texturesNode)
	{
		auto subNode = texturesNode->first_node();
		while (subNode)
		{
			auto name = subNode->first_attribute("name");
			auto path = subNode->first_attribute("path");

			if (name && path)
			{
				loadTexture(name->value(), m_rootFolder + path->value());
			}

			subNode = subNode->next_sibling();
		}
	}
	else
	{
		spdlog::error("Unable to find texture node in assets.xml");
	}

	// Shaders
	auto shadersNode = parentNode->first_node("shaders");
	if (shadersNode)
	{
		auto subNode = shadersNode->first_node();
		while (subNode)
		{
			auto name = subNode->first_attribute("name");
			auto vertPath = subNode->first_attribute("vert_path");
			auto fragPath = subNode->first_attribute("frag_path");

			if (name && vertPath && fragPath)
			{
				loadShader(name->value(), m_rootFolder + vertPath->value(), m_rootFolder + fragPath->value());
			}
			subNode = subNode->next_sibling();
		}
	}
	else
	{
		spdlog::error("Unable to find shaders node in assets.xml");
	}

	// Sounds
	auto soundsNode = parentNode->first_node("sounds");
	if (soundsNode)
	{
		auto subNode = soundsNode->first_node();
		while (subNode)
		{
			auto name = subNode->first_attribute("name");
			auto path = subNode->first_attribute("path");

			if (name && path)
			{
				loadSound(name->value(), m_rootFolder + path->value());
			}
			subNode = subNode->next_sibling();
		}
	}
	else
	{
		spdlog::error("Unable to find sounds node in assets.xml");
	}

	//Tiled Maps
	auto tiledMapsNode = parentNode->first_node("tiledmaps");
	if (tiledMapsNode)
	{
		auto subNode = tiledMapsNode->first_node();
		while (subNode)
		{
			auto name = subNode->first_attribute("name");
			auto path = subNode->first_attribute("path");

			if (name && path)
			{
				loadTilemap(name->value(), m_rootFolder + path->value());
			}

			subNode = subNode->next_sibling();
		}
	}
	else
	{
		spdlog::error("Unable to find tiledmaps node in assets.xml");
	}

	// Fonts
	auto fontsNode = parentNode->first_node("fonts");
	if (fontsNode)
	{
		auto subNode = fontsNode->first_node();
		while (subNode)
		{
			auto name = subNode->first_attribute("name");
			auto path = subNode->first_attribute("path");

			if (name && path)
			{
				loadFont(name->value(), m_rootFolder + path->value());
			}

			subNode = subNode->next_sibling();
		}
	}
	else
	{
		spdlog::error("Unable to find fonts node in assets.xml");
	}

	auto musicNode = parentNode->first_node("music");
	if (musicNode)
	{
		auto subNode = musicNode->first_node();
		while (subNode)
		{
			auto name = subNode->first_attribute("name");
			auto path = subNode->first_attribute("path");

			if (name && path)
			{
				loadMusic(name->value(), m_rootFolder + path->value());
			}

			subNode = subNode->next_sibling();
		}
	}
	else
	{
		spdlog::error("Unable to find music node in assets.xml");
	}
}

void AssetLoader::loadAnimationsXML()
{
	ifstream animationsXMLFile;
	const string filePath = m_rootFolder + L"\\animations.xml";
	animationsXMLFile.open(filePath, ios::in);
	//animationsXMLFile >> noskipws;

	std::stringstream buffer;
	buffer << animationsXMLFile.rdbuf();

	if (animationsXMLFile.fail())
	{
		spdlog::error("No animations.xml file found!");
		return;
	}

	std::string xmlFileAsStr;
	xmlFileAsStr = buffer.str();
	int32 index = 0;

	animationsXMLFile.close();

	xml_document<> animationsXMLDoc;
	animationsXMLDoc.parse<0>(&xmlFileAsStr[0]);
	xml_node<> *animation_node = animationsXMLDoc.first_node("data")->first_node("animation");
	anim::AnimationDef animDataStruct;

	if (!animation_node)
	{
		spdlog::error("No animation nodes found in {}", filePath);
		goto cleanup_branch_fail;
	}

	xml_attribute<> *pAnimationName = nullptr, *pTextureName = nullptr, *pFrameTime = nullptr,
					*pWidth = nullptr, *pHeight = nullptr;

	while (animation_node)
	{
		//Animation name
		{
			std::string temp = animation_node->name();
			if (temp != "animation")
			{
				spdlog::error("Incorrect node name {}", animation_node->name());
				goto cleanup_branch_fail;
			}
			pAnimationName = animation_node->first_attribute("name");
			if (!pAnimationName)
			{
				spdlog::error("No animation name specified");
				goto cleanup_branch_fail;
			}

			if (strlen(pAnimationName->value()) == 0)
			{
				spdlog::error("Invalid animation name, length == 0");
				goto cleanup_branch_fail;
			}

			animDataStruct.animationName = pAnimationName->value();
		}

		//Texture name
		{
			pTextureName = animation_node->first_attribute("texture_name");
			if (!pTextureName)
			{
				spdlog::error("No texture name referenced, animation name: {}", animDataStruct.animationName);
				goto cleanup_branch_fail;
			}
			if (strlen(pTextureName->value()) == 0)
			{
				spdlog::error("Invalid texture name! Texture name length == 0! anim name: {}", animDataStruct.animationName);
				goto cleanup_branch_fail;
			}
			animDataStruct.textureName = pTextureName->value();
		}

		//Frame Time
		{
			pFrameTime = animation_node->first_attribute("fps");
			if (!pFrameTime)
			{
				spdlog::error("No fps referenced, animation name: {}", animDataStruct.animationName);
				goto cleanup_branch_fail;
			}
			if (strlen(pTextureName->value()) == 0)
			{
				spdlog::error("No value given for fps, animation name: {}", animDataStruct.animationName);
				goto cleanup_branch_fail;
			}
			const float denominator = static_cast<float>(atof(pFrameTime->value()));
			if (denominator != 0.0f)
			{
				animDataStruct.frameTime = 1.0f / denominator;
			}
		}

		//bounds
		{

			pWidth = animation_node->first_attribute("width");
			if (!pWidth)
			{
				spdlog::error("No width referenced, animation name: {}", animDataStruct.animationName);
				goto cleanup_branch_fail;
			}
			if (strlen(pWidth->value()) == 0)
			{
				spdlog::error("Width property length == 0, animation name: {}", animDataStruct.animationName);
				goto cleanup_branch_fail;
			}

			animDataStruct.bounds.x = (float)atoi(pWidth->value());
			pHeight = animation_node->first_attribute("height");

			if (!pHeight)
			{
				spdlog::error("No height referenced, animation name: {}", animDataStruct.animationName);
				goto cleanup_branch_fail;
			}

			if (strlen(pHeight->value()) == 0)
			{
				spdlog::error("height property length == 0, animation name: ", animDataStruct.animationName);
				goto cleanup_branch_fail;
			}
			animDataStruct.bounds.y = (float)atoi(pHeight->value());
		}

		xml_node<> *pFrameNode = animation_node->first_node("frame");
		xml_attribute<> *pXPosAttr = nullptr, *pYPosAttr = nullptr;
		Vec2f frameData;
		while (pFrameNode)
		{

			pXPosAttr = pFrameNode->first_attribute(("x_pos"));
			if (pXPosAttr)
			{
				if (strlen(pXPosAttr->value()) > 0)
				{
					frameData.x = (float)atoi(pXPosAttr->value());
				}
				else
				{
					spdlog::error("Invalid frame x position! anim name {}", animDataStruct.animationName);
				}
			}
			else
			{
				spdlog::error("Invalid frame x position! anim name {}", animDataStruct.animationName);
			}

			pYPosAttr = pFrameNode->first_attribute("y_pos");
			if (pYPosAttr)
			{
				if (strlen(pYPosAttr->value()) > 0)
				{
					frameData.y = (float)atoi(pYPosAttr->value());
				}
				else
				{
					spdlog::error("Invalid frame y position! anim name {}", animDataStruct.animationName);
				}
			}
			else
			{
				spdlog::error("Invalid frame y position! anim name {}", animDataStruct.animationName);
			}

			animDataStruct.frameData.push_back(frameData);

			pFrameNode = pFrameNode->next_sibling();
		}
		m_animationsMap.emplace(animDataStruct.animationName, new anim::AnimationDef(animDataStruct));

		animation_node = animation_node->next_sibling();
		animDataStruct.frameData.clear();
	}

cleanup_branch_fail:
	animationsXMLDoc.clear();
	return;
}

void AssetLoader::matchAnimationsToTextures()
{
	for (auto &anim : m_animationsMap)
	{
		Texture *pTex = getTexture(anim.second->textureName);
		if (!pTex)
		{
			spdlog::error("Unable to find texture {} for animation {}", anim.second->textureName, anim.first);
			continue;
		}
		anim.second->pTexture = pTex;
	}
}
