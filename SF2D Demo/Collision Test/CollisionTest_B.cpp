#include "stdafx.h"

// stl
#include <array>
#include <memory>

// engine
#include <Krawler.h>
#include <KApplication.h>
#include <AssetLoader\KAssetLoader.h>
#include <Utilities\KDebug.h>

#include <Components\KCBoxCollider.h>
#include <Components\KCCircleCollider.h>
#include <Components\KCBody.h>

//external
#include "imgui-SFML.h"
#include "imgui.h"
#include <SFML/Graphics/ConvexShape.hpp>
#ifdef _DEBUG
// CRT 
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

using namespace sf2d;
using namespace sf2d::Input;
using namespace sf2d::Components;
using namespace sf2d::Maths;

void processEvent(const sf::Event& e)
{
	ImGui::SFML::ProcessEvent(e);
}

// custom imgui component
class imguicomp : public KComponentBase
{
public:
	imguicomp(KEntity* pEntity) : KComponentBase(pEntity) {}
	~imguicomp() = default;

	virtual KInitStatus init() override
	{
		ImGui::SFML::Init(*KApplication::getApp()->getRenderWindow());
		std::function<void(void)> subLastDraw = std::bind(&imguicomp::draw, this);
		KApplication::getApp()->subscribeToEventQueue(processEvent);
		KApplication::getApp()->getRenderer()->subscribeLastDrawCallback(subLastDraw);
		m_bWasInitSuccessful = true;
		return KInitStatus::Success;
	}

	virtual void cleanUp() override
	{
		ImGui::SFML::Shutdown();
	}

	// call to invoke imgui::begin
	void begin(const std::string& name)
	{
		if (!m_bBeginCalled)
		{
			ImGui::Begin(name.c_str());
			m_bBeginCalled = true;
		}
	}

	// call to invoke imgui::end
	void end()
	{
		if (!m_bEndCalled)
		{
			m_bEndCalled = true;
			ImGui::End();
		}
	}

	// Call to invoke imgui::sfml::update
	void update()
	{
		if (!m_bUpdateRun)
		{
			ImGui::SFML::Update(*KApplication::getApp()->getRenderWindow(), sf::seconds(1.0f / (float)(KApplication::getApp()->getGameFPS())));
			m_bUpdateRun = true;
		}
	}

private:

	void draw()
	{
		// If init is unsuccessful we'll avoid invoking render since this can force an abrupt halt 
		// instead we'll print that we're skipping an imgui render call 
		if (m_bWasInitSuccessful)
		{
			static bool hasBeenPrint = false;
			if (!hasBeenPrint)
			{
				//KPRINTF("Skipping imgui draw");
				hasBeenPrint = true;
			}
		}

		// If the update hasn't been run yet, we should totally avoid calling imgui draw
		// this is an applicating halting mistake to make.
		if (!m_bUpdateRun)
		{
			return;
		}


		// If begin was not called, do not try render. 
		// This is an application halting mistake to make.
		if (!m_bBeginCalled)
		{
			return;
		}

		// If end was not called, do not try render. 
		// This is an application halting mistake to make.
		if (!m_bEndCalled)
		{
			return;
		}

		ImGui::SFML::Render(*KApplication::getApp()->getRenderWindow());
		m_bUpdateRun = false;
		m_bBeginCalled = false;
		m_bEndCalled = false;
	}

	bool m_bUpdateRun = false;
	bool m_bWasInitSuccessful = false;
	bool m_bBeginCalled = false;
	bool m_bEndCalled = false;
};

class Box2DComp : public KComponentBase
{
public:

	Box2DComp(KEntity* pEntity) :
		KComponentBase(pEntity)
	{

	}

	virtual KInitStatus init() override
	{
		const Vec2f BOX_BOUNDS(50.0f, 50.0f);
		const Vec2f FLOOR_BOUNDS(KCAST(float, GET_APP()->getWindowSize().x), 50);
		KScene* const pScene = GET_SCENE();

		spdlog::set_level(spdlog::level::debug);

		for (int32 i = 0; i < BOX_COUNT; ++i)
		{
			auto const testBox = pScene->addEntityToScene();
			m_pBox = testBox;
			testBox->addComponent(new KCSprite(testBox, BOX_BOUNDS));
			auto& trans = *testBox->getTransform();

			const Vec2f RandPos(Maths::RandFloat(0, 250), Maths::RandFloat(0, 250));
			trans.setPosition(RandPos);
			trans.setOrigin(BOX_BOUNDS * 0.5f);
			//trans.setRotation(Maths::RandFloat(0, 359));
			auto collider = new KCBoxCollider(testBox, Vec2f(BOX_BOUNDS));
			//auto collider = new KCCircleCollider(testBox, BOX_BOUNDS.x / 2.0f);
			testBox->addComponent(collider);
			m_boxes.push_back(testBox);
			if (i == 0)
				collider->subscribeCollisionCallback(&m_callback);
		}

		GET_APP()->getRenderer()->addDebugShape(&m_shape);
		GET_APP()->getRenderer()->showDebugDrawables(true);
		return KInitStatus::Success;
	}

	virtual void onEnterScene() override
	{
		for (auto& box : m_boxes)
		{
			uint8 r = static_cast<uint8>(RandInt(256));
			uint8 g = static_cast<uint8>(RandInt(256));
			uint8 b = static_cast<uint8>(RandInt(256));

			box->getComponent<KCSprite>()->setColour(sf::Color(r, g, b));
		}
	}

	virtual void tick() override
	{
		static bool bOpen = true;
		auto imgui = m_entity->getComponent<imguicomp>();
		imgui->update();
		imgui->begin("Box2D Testing");
		auto button = ImGui::Button("Toggle Debug Shapes");
		if (button)
		{
			auto r = GET_APP()->getRenderer();
			r->showDebugDrawables(!r->isShowingDebugDrawables());
		}
		imgui->end();

		for (auto& b : m_boxes)
		{
			//b->getComponent<KCSprite>()->setColour(Colour::White);
		}

		/*m_boxes[0]->getTransform()->setPosition(KInput::GetMouseWorldPosition());
		m_boxes[0]->getComponent<KCSprite>()->setColour(sf::Color::White);*/

		auto& b = m_boxes[0];
		float dt = GET_APP()->getDeltaTime();
		float mov = 50.0f;
		if (KInput::Pressed(KKey::W))
		{
			b->m_pTransform->move(0.0f, -mov * dt);
		}

		if (KInput::Pressed(KKey::S))
		{
			b->m_pTransform->move(0.0f, mov * dt);
		}

		if (KInput::Pressed(KKey::A))
		{
			b->m_pTransform->move(-mov * dt, 0.0f);
		}

		if (KInput::Pressed(KKey::D))
		{
			b->m_pTransform->move(mov * dt, 0.0f);
		}

		/*auto mousePosition = KInput::GetMouseWorldPosition();
		b->m_pTransform->setPosition(mousePosition);*/
		b->getComponent<KCSprite>()->setColour(sf::Color::White);

		auto& b2 = m_boxes[1];

		m_shape = sf::ConvexShape(4);
		auto col = b2->getComponent<KCBoxCollider>();
		auto shapeBox2D = col->getB2Shape();
		auto box2Dcol = std::dynamic_pointer_cast<b2PolygonShape>(shapeBox2D.lock());
		auto v = box2Dcol->m_vertices;
		for (int32 i = 0; i < 4; ++i)
		{
			Vec2f pos = Vec2f(v[i].x, v[i].y);
			//pos = b2->m_pTransform->getTransform().transformPoint(pos);
			
			m_shape.setPoint(i, pos);
		}
		m_shape.setFillColor(sf::Color::Cyan);


		if (KInput::Pressed(KKey::A))
		{
			b2->m_pTransform->rotate(-20.0f * dt);
		}

		if (KInput::Pressed(KKey::D))
		{
			b2->m_pTransform->rotate(20.0f * dt);
		}
	}

private:
	const int32 BOX_COUNT = 2;

	KEntity* m_pBox = nullptr;
	std::vector<KEntity*> m_boxes;

	ColliderBaseCallback m_callback = [this](const KCollisionDetectionData& collData)
	{

		//std::cout << "setting green" << std::endl;
		//spdlog::debug("Normal = {} | {}", collData.collisionNormal.x, collData.collisionNormal.y);
		float dt = GET_APP()->getDeltaTime();
		collData.entityA->m_pTransform->move(collData.collisionNormal * 60.0f * dt);

		if (collData.entityA == m_boxes[0])
			collData.entityA->getComponent<KCSprite>()->setColour(sf::Color::Green);
		else
			collData.entityB->getComponent<KCSprite>()->setColour(sf::Color::Green);
	};

	sf::ConvexShape m_shape;

};

#ifndef _CONSOLE
#include <Windows.h>
int WINAPI WinMain(HINSTANCE hThisInstance, HINSTANCE hPrevInstance, LPSTR lpszArgument, int nCmdShow)
#else
int main(void)
#endif
{
	int32 i = sf::Texture::getMaximumSize();
	KApplicationInitialise initApp(false);
	initApp.gameFps = 60;
	initApp.physicsFps = 60;
	initApp.width = 1024;//sf::VideoMode::getDesktopMode().width;
	initApp.height = 768; // sf::VideoMode::getDesktopMode().height;
	initApp.windowStyle = KWindowStyle::Windowed_Fixed_Size;
	initApp.windowTitle = "Collision Module Check";
	StartupEngine(&initApp);
	//const int sceneWidth = KAssetLoader::getAssetLoader().getLevelMap(L"test_level")->width;
	//const int sceneHeight = KAssetLoader::getAssetLoader().getLevelMap(L"test_level")->height;
	auto app = KApplication::getApp();
	app->getSceneDirector().addScene(new KScene(std::string("Main_Scene"), Rectf(0, 0, (70 * 32), (40 * 32))));
	app->getSceneDirector().addScene(new KScene(std::string("Test_Scene"), Rectf(0, 0, (70 * 32), (40 * 32))));
	app->getSceneDirector().setStartScene("Main_Scene");
	auto entity = GET_SCENE()->addEntityToScene();
	entity->addComponent(new Box2DComp(entity));
	entity->addComponent(new imguicomp(entity));
	//DO STUFF WITH ENTITY HERE
	InitialiseSubmodules();

	RunApplication();

	ShutdownEngine();
	_CrtDumpMemoryLeaks();
	return 0;
}

