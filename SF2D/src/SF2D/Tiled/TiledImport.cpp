#include <SF2D/Tiled/TiledImport.h>

#include <fstream>
#include <sstream>
#include <wchar.h> 

#include <nlohmann/json.hpp>

#include <SFML/System/String.hpp>

using namespace sf2d;
using namespace sf2d::tiled;
using namespace std;

using json = nlohmann::json;

/*
@Remember String Conversion Method
json.hpp libray uses std::string, while the internals of KRAWLER use std::string. Therefore a horrid workaround
is to use sf::String(string_object).toWideString().
*/
//to wide string conversion via SFML

const std::string MAP_PARSE_ERR = "TILED MAP JSON PARSE ERROR: ";
#define DOES_ELEMENT_EXIST(name, jsonObj) (jsonObj.count(name) > 0)
#define GET_NUMBER_FLOAT(jsonElement, name, floatVar) \
if (jsonElement[name].is_number_integer())\
{\
	floatVar = (float)jsonElement[name].get<int32>();\
}\
else if( jsonElement[name].is_number_unsigned()) \
{\
	floatVar = (float)jsonElement[name].get<uint32>();\
}\
else \
{\
	floatVar = jsonElement[name].get<float>();\
}\

#define GET_NUMBER_INT(jsonElement, name, intVar) \
if (jsonElement[name].is_number_integer())\
{\
	intVar = jsonElement[name].get<int32>();\
}\
else if( jsonElement[name].is_number_unsigned()) \
{\
	intVar = (signed)jsonElement[name].get<uint32>();\
}\
else \
{\
	intVar = (int)jsonElement[name].get<float>();\
}\

// --- FUNCTION DECLERATIONS --- \\

//Desc: Is the the map file valid in terms of type (has a property called 'type' with a value of 'map') 
//Params: map json object
//Return: true if valid, false if invalid
static bool is_valid_level_map_type(const json& rootJson);

//Desc: Is there a layers array attached to the object
//Params: map json object
//Return: true if present, false if not
static bool are_map_layers_present(const json& rootJson);

//Desc: Load tiled map meta data, and handle all layer extraction & tileset data
//Params: json object of map, KTIMap pointer
//Return: true if successful, false if failed
static bool load_tiled_map(const json& rootJson, MapRoot* pMap);

//Desc: get a string value if it present on a json object
//Params: string ref of variable to be set, name of json object, json object to extract from
//Return: true if set, false if not
static bool get_string_if_present(string& value, const string& name, const json& jsonObj);

//Desc: get a int value if it present on a json object
//Params: float ref of variable to be set, name of json object, json object to extract from
//Return: true if set, false if not
static bool get_int_if_present(int32& value, const string& name, const json& rootJson);

//Desc: get a int value if it present on a json object
//Params: float ref of variable to be set, name of json object, json object to extract from
//Return: true if set, false if not
static bool get_uint_if_present(uint32& value, const string& name, const json& rootJson);

//Desc: get a float value if it present on a json object
//Params: float ref of variable to be set, name of json object, json object to extract from
//Return: true if set, false if not
static bool get_float_if_present(float& value, const string& name, const json& rootJson);

//Desc: Extract properties from any json node that has properties
//Params: json object, KTIPropertiesMap ref, KTIPropertyTypesMap ref
//Return: N/A
static void extract_properties_to_map(const json& jsonObj, PropertiesMap& propMap, PropertyTypesMap& typeMap);

//Desc: Extract tiled and object layers and add them to a KTIMap
//Params: json object (containing layers array member), pointer to KTI map
//Return: true if successful, false if not
static bool extract_map_layers(const json& jsonObj, MapRoot* pMap);

//Desc: Returns enum of the type of a property based on its name
//Params: string of name
//Return: KTIPropertyTypes enum of property type (will default to string if not known) 
static PropertyTypes get_property_type_by_string(const std::string& name);

//Desc: Returns enum type of the layer 
//Params: json object which contains a field 'type' for layer type
//Return: KTILayerTypes enum 
static LayerTypes get_layer_type(const json& layerJsonObj);

//Desc: Is the object we've identified a template of an object
//Params: json obj
//Return: true if is template, false if not
static bool is_template_object(const json& mapObjectJson);

//Desc: Fill out a KTILayer object with the data for an Tiled 'object layer' 
//Params: json object (containing object layer data), ptr to KTILayer
//Return: N/A
static void extract_object_layer_data(const json& objectLayerJson, Layer* pObjLayerData);

//Desc: Fill out the relevant fields of a KTIObject
//Params: json object (containing Tiled object data), ptr to KTIObject
//Return: N/A
static void extract_object_data(const json& objectsArray, Object* pObj);

//Desc: 
//Params:
//Return:
static void extract_tile_layer_data(const json& tileLayerJson, Layer* pTileLayerData);

//Desc: 
//Params:
//Return:
static bool extract_tile_sets(const json& tileLayerJson, MapRoot* pMap);

//Desc: 
//Params:
//Return:
static bool extract_singular_tileset(const json& tilesetJson, Tileset* pTilesetData);

//Desc: 
//Params:
//Return:
static void extract_tile_properties(const json& jsonObj, std::map<std::string, PropertiesMap>& tilePropMap, std::map<std::string, PropertyTypesMap>&);

//--- PUBLIC FUNCTION DEFINITIONS --- \\

MapRoot* sf2d::tiled::loadTiledJSONFile(const std::string filePath)
{
	MapRoot* pMap = new MapRoot;

	ifstream jsonFile;
	jsonFile.open(filePath, ios::in);

	if (jsonFile.fail())
	{
		return nullptr;
	}

	json rootObject;
	jsonFile >> rootObject;

	if (!is_valid_level_map_type(rootObject))
	{
		spdlog::error("Invalid root object json type in in tiledmap {}", filePath);
		KFREE(pMap);
		return nullptr;
	}

	const bool load_status = load_tiled_map(rootObject, pMap);

	if (!load_status)
	{
		KFREE(pMap);
		return nullptr;
	}
	jsonFile.close();
	rootObject.clear();
	return pMap;
}

void sf2d::tiled::cleanupLevelMap(MapRoot* pMap)
{
	pMap->layersVector.clear();
	pMap->tilesetVector.clear();
	KFREE(pMap);
}

// -- STATIC FUNCTION DEFINITIONS  -- \\

bool is_valid_level_map_type(const json& rootJson)
{
	auto findResult = rootJson.count("type");
	if (findResult == 0)
	{
		return false;
	}
	const string&& p = rootJson["type"].get<string>();
	return p == "map";
}

bool are_map_layers_present(const json& rootJson)
{
	return rootJson.count("layers") > 0;
}

bool load_tiled_map(const json& rootJson, MapRoot* pMap)
{
	KCHECK(pMap);

	if (!are_map_layers_present(rootJson))
	{
		spdlog::error("{} No layers attached to map!", MAP_PARSE_ERR);
		return false;
	}

	if (!get_uint_if_present(pMap->width, "width", rootJson))
	{
		spdlog::error("{} Failed to find 'width' field on map!", MAP_PARSE_ERR);
		return false;
	}

	if (!get_uint_if_present(pMap->height, "height", rootJson))
	{
		spdlog::error("{} Failed to find 'height' field on map!", MAP_PARSE_ERR);
		return false;
	}

	if (!get_uint_if_present(pMap->tileWidth, "tilewidth", rootJson))
	{
		spdlog::error("{} Failed to find 'tilewidth' field on map!", MAP_PARSE_ERR);
		return false;
	}

	if (!get_uint_if_present(pMap->tileHeight, "tileheight", rootJson))
	{
		spdlog::error("{} Failed to find 'tileheight' field on map!", MAP_PARSE_ERR);
		return false;
	}

	if (!get_uint_if_present(pMap->nextObjectID, "nextobjectid", rootJson))
	{
		spdlog::error("{} Failed to find 'nextobjectid' field on map!", MAP_PARSE_ERR);
		return false;
	}

	extract_properties_to_map(rootJson, pMap->properties, pMap->propertyTypes);

	if (!extract_map_layers(rootJson, pMap))
	{
		return false;
	}

	if (!extract_tile_sets(rootJson, pMap))
	{
		return false;
	}

	return true;
}

bool get_int_if_present(int32& value, const string& name, const json& rootJson)
{
	auto intProperty = rootJson.count(name);
	if (intProperty == 0)
	{
		return false;
	}

	auto result = rootJson[name];

	if (!result.is_number_integer())
	{
		return false;
	}
	value = result.get<int>();
	return true;
}

bool get_uint_if_present(uint32& value, const string& name, const json& rootJson)
{
	auto intProperty = rootJson.count(name);
	if (intProperty == 0)
	{
		return false;
	}

	auto result = rootJson[name];

	if (!result.is_number_integer())
	{
		return false;
	}
	value = result.get<uint32>();
	return true;
}

bool get_float_if_present(float& value, const string& name, const json& rootJson)
{
	auto floatProp = rootJson.count(name);
	if (floatProp == 0)
	{
		return false;
	}

	auto result = rootJson[name];

	if (!result.is_number_float())
	{
		return false;
	}
	value = result.get<float>();
	return true;
}

void extract_properties_to_map(const json& jsonObj, PropertiesMap& propMap, PropertyTypesMap& typeMap)
{
	auto property_obj_it = jsonObj.find("properties");
	//auto property_types_it = jsonObj.find("propertytypes");

	if (property_obj_it == jsonObj.end())
	{ // not always likely to find properties, so return true
		return;
	}

	//Get total number of properties 
	const uint32 Property_Count = static_cast<uint32>(property_obj_it->size());


	//@UrgentRefactor switch statement to do function calls, and also isolate out the functionality to isolate properties so its generic for other map object types which will have properties.
	json::value_type properties_obj = *property_obj_it;

	json::iterator itProperties = properties_obj.begin();

	//iterate through all properties & property-types objects
	while (itProperties != properties_obj.end())
	{
		std::string tempStr = itProperties.value().find("type").value().get<string>();
		const std::string Type_Name = tempStr;
		const PropertyTypes Type_Enum = get_property_type_by_string(Type_Name); //isolate the data type of this property 

		Property mapPropertyUnion{ 0 };
		bool bLoadedCorrectly = true;
		std::string key, type;

		key = itProperties.value().find("name").value().get<string>();

		switch (Type_Enum)
		{
		case PropertyTypes::String:
		{
			tempStr = itProperties.value().find("value").value().get<string>();
			if (tempStr.size() < MAX_PROPERTY_STRING_CHARS)
			{
				strcpy_s(mapPropertyUnion.type_string, MAX_PROPERTY_STRING_CHARS, tempStr.c_str());
				propMap.emplace(key, mapPropertyUnion);
			}
			else
			{
				bLoadedCorrectly = false;
				spdlog::error("{}  Unable to load string property {} from JSON as string is too large!", MAP_PARSE_ERR, tempStr);
			}
		}
		break;

		case PropertyTypes::Int:
		{
			mapPropertyUnion.type_int = itProperties.value().find("value").value().get<int>();
			propMap.emplace(key, mapPropertyUnion);
		}
		break;

		case PropertyTypes::Float:
		{
			mapPropertyUnion.type_float = itProperties.value().find("value").value().get<float>();
			propMap.emplace(key, mapPropertyUnion);
		}
		break;

		case PropertyTypes::Bool:
		{
			mapPropertyUnion.type_bool = itProperties.value().find("value").value().get<bool>();
			propMap.emplace(key, mapPropertyUnion);
		}
		break;

		case PropertyTypes::HexColour:
		{
			//@Rethink Horrible way of parsing hex value of colour
			tempStr = itProperties.value().find("value").value().get<string>();

			/*
			Colours are represented by tiled as
			# ALPHA_BITS RED_BITS BLUE_BITS GREEN_BITS
			Example
			#ff2d0044 => # FF 2d 00 44 (or 45,0,68,255 as RGBA)

			To parse this correctly 4 strings contain each channels hex string representation of the value.
			This is then put through a stringstream to allow conversion directly to int
			*/

			std::string alphaChannelChars, redChannelHexChars, greenChannelHexChars, blueChannelHexChars;
			alphaChannelChars.push_back(tempStr[1]);
			alphaChannelChars.push_back(tempStr[2]);

			redChannelHexChars.push_back(tempStr[3]);
			redChannelHexChars.push_back(tempStr[4]);

			greenChannelHexChars.push_back(tempStr[5]);
			greenChannelHexChars.push_back(tempStr[6]);

			blueChannelHexChars.push_back(tempStr[7]);
			blueChannelHexChars.push_back(tempStr[8]);

			int32 AlphaValue, RedValue, BlueValue, GreenValue;
			stringstream ss;
			ss << std::hex << alphaChannelChars;
			ss >> AlphaValue;
			ss.clear();

			ss << std::hex << redChannelHexChars;
			ss >> RedValue;
			ss.clear();

			ss << std::hex << blueChannelHexChars;
			ss >> BlueValue;
			ss.clear();

			ss << std::hex << greenChannelHexChars;
			ss >> GreenValue;
			ss.clear();
			mapPropertyUnion.type_colour = Colour((int8)RedValue, (int8)GreenValue, (int8)BlueValue, (int8)AlphaValue);

			propMap.emplace(key, mapPropertyUnion);
			break;
		}

		case PropertyTypes::File:
		{
			bLoadedCorrectly = false;
			spdlog::error("{} File property not supported in this parser!", MAP_PARSE_ERR);
		}
		break;

		default:
			break;
		}

		if (bLoadedCorrectly)
		{
			typeMap.emplace(key, Type_Enum);
		}
		++itProperties;
	}
}

bool extract_map_layers(const json& jsonObj, MapRoot* pMap)
{
	KCHECK(pMap);
	auto layersObject = jsonObj["layers"];

	KCHECK(layersObject.is_array()); // layers must always be an array 

	const uint64 LayerCount = layersObject.size();

	// No layers == invalid file 
	if (LayerCount == 0)
	{
		spdlog::error("{} No layers found on tiledmap file!", MAP_PARSE_ERR);
		return false;
	}

	pMap->layersVector.resize(LayerCount);

	for (uint32 i = 0; i < LayerCount; ++i)
	{
		auto& individual_layer_struct = pMap->layersVector[i];
		pMap->layersVector[i].layerType = get_layer_type(layersObject[i]);

		if (!get_string_if_present(individual_layer_struct.name, "name", layersObject[i]))
		{
			spdlog::error("{} No name found on tiledmap layer!", MAP_PARSE_ERR);
			continue;
		}

		//float handle positions
		if (!DOES_ELEMENT_EXIST("x", layersObject[i]))
		{
			spdlog::error("{} No x position found on tiledmap layer!", MAP_PARSE_ERR);
		}

		GET_NUMBER_FLOAT(layersObject[i], "x", individual_layer_struct.x);

		if (!DOES_ELEMENT_EXIST("y", layersObject[i]))
		{
			spdlog::error("{} No y position found on tiledmap layer!", MAP_PARSE_ERR);
		}

		GET_NUMBER_FLOAT(layersObject[i], "y", individual_layer_struct.y);

		//handle offsets
		if (DOES_ELEMENT_EXIST("offsetx", layersObject[i]))
		{
			GET_NUMBER_FLOAT(layersObject[i], "offsetx", individual_layer_struct.offsetX);
		}

		if (DOES_ELEMENT_EXIST("offsety", layersObject[i]))
		{
			GET_NUMBER_FLOAT(layersObject[i], "offsety", individual_layer_struct.offsetY);
		}

		switch (pMap->layersVector[i].layerType)
		{
		case LayerTypes::TileLayer:
			extract_tile_layer_data(layersObject[i], &individual_layer_struct);
			break;

		case LayerTypes::ObjectLayer:

			extract_object_layer_data(layersObject[i], &individual_layer_struct);
			break;

		default:
			break;
		}
	}
	return true;
}

PropertyTypes get_property_type_by_string(const std::string& name)
{
	PropertyTypes type = PropertyTypes::String; // default type

	if (name == "string")
	{
		return PropertyTypes::String;
	}
	else if (name == "int")
	{
		return PropertyTypes::Int;
	}
	else if (name == "float")
	{
		return PropertyTypes::Float;
	}
	else if (name == "bool")
	{
		return PropertyTypes::Bool;
	}
	else if (name == "color") //silly American spelling..
	{
		return PropertyTypes::HexColour;
	}
	else if (name == "file")
	{
		return PropertyTypes::File;
	}
	return type;
}

LayerTypes get_layer_type(const json& layerJsonObj)
{
	if (layerJsonObj["type"].get<string>() == "objectgroup")
	{
		return LayerTypes::ObjectLayer;
	}
	return LayerTypes::TileLayer;
}

bool is_template_object(const json& mapObjectJson)
{
	return mapObjectJson.count("template") > 0;
}

void extract_object_layer_data(const json& objectLayerJson, Layer* pObjLayerData)
{
	//extract layer level properties 
	extract_properties_to_map(objectLayerJson, pObjLayerData->propertiesMap, pObjLayerData->propertyTypesMap);

	if (!objectLayerJson.is_object())
	{
		return;
	}

	if (!objectLayerJson["objects"].is_array())
	{
		return;
	}

	const uint64 ObjectCount = objectLayerJson["objects"].size();
	const json& mapObjectsArrayJson = objectLayerJson["objects"];

	if (ObjectCount < 1)
	{
		return;
	}

	pObjLayerData->objectsVector.resize(ObjectCount);

	KCHECK(pObjLayerData->objectsVector.size() > 0);

	for (int32 i = 0; i < ObjectCount; ++i)
	{
		extract_object_data(mapObjectsArrayJson[i], &pObjLayerData->objectsVector[i]);
	}
}

void extract_object_data(const json& objectsArray, Object* pObj)
{
	if (!objectsArray.count("name"))
	{
		spdlog::error("{} Object lacking name field!", MAP_PARSE_ERR);
		return;
	}

	pObj->name = objectsArray["name"].get<string>();

	if (!objectsArray.count("x"))
	{
		spdlog::error("{} Object lacking x position (name %s)!", MAP_PARSE_ERR, pObj->name);
		return;
	}

	GET_NUMBER_FLOAT(objectsArray, "x", pObj->x);

	if (!objectsArray.count("y"))
	{
		spdlog::error("{} Object lacking y position (name %s)!", MAP_PARSE_ERR, pObj->name);
		return;
	}
	GET_NUMBER_FLOAT(objectsArray, "y", pObj->y);

	if (objectsArray.count("gid") > 0)
	{
		if (objectsArray["gid"].is_number_integer())
		{
			pObj->gid = objectsArray["gid"].get<int>();
		}
	}

	if (objectsArray["id"].is_number_integer())
	{
		pObj->id = objectsArray["id"].get<int>();
	}

	GET_NUMBER_FLOAT(objectsArray, "rotation", pObj->rotation);

	extract_properties_to_map(objectsArray, pObj->propertiesMap, pObj->propertyTypesMap);

	// Extract the objects type 

	if (objectsArray.count("point") > 0)
	{
		pObj->objectType = ObjectTypes::Point;
	}
	else if (objectsArray.count("ellipse") > 0)
	{
		pObj->objectType = ObjectTypes::Circle;
		GET_NUMBER_FLOAT(objectsArray, "width", pObj->width);
		GET_NUMBER_FLOAT(objectsArray, "height", pObj->height);
	}
	else if (objectsArray.count("polygon") > 0)
	{
		pObj->objectType = ObjectTypes::Polygon;
		//@Remember Polygon logic no implemented
	}
	else
	{
		pObj->objectType = ObjectTypes::Rect;
		GET_NUMBER_FLOAT(objectsArray, "width", pObj->width);
		GET_NUMBER_FLOAT(objectsArray, "height", pObj->height);
	}
}

void extract_tile_layer_data(const json& tileLayerJson, Layer* pTileLayerData)
{
	if (!DOES_ELEMENT_EXIST("width", tileLayerJson))
	{
		spdlog::error("{} No width on tile layer!", MAP_PARSE_ERR);
		return;

	}

	GET_NUMBER_FLOAT(tileLayerJson, "width", pTileLayerData->width);

	if (!DOES_ELEMENT_EXIST("height", tileLayerJson))
	{
		spdlog::error("{} No height on tile layer!", MAP_PARSE_ERR);
		return;
	}

	GET_NUMBER_FLOAT(tileLayerJson, "height", pTileLayerData->height);

	const sf2d::int32 TILE_TOTAL = static_cast<int32>(pTileLayerData->width * pTileLayerData->height);

	if (tileLayerJson["data"].size() != TILE_TOTAL)
	{
		spdlog::error("{} Tile array size doesn't match width * height", MAP_PARSE_ERR);
		return;
	}

	auto&& arr = tileLayerJson["data"].get<std::vector<sf2d::int32>>();
	pTileLayerData->tileData.resize(TILE_TOTAL);
	memcpy_s(&pTileLayerData->tileData[0], sizeof(int32) * TILE_TOTAL, &arr[0], sizeof(int32) * TILE_TOTAL);

	extract_properties_to_map(tileLayerJson, pTileLayerData->propertiesMap, pTileLayerData->propertyTypesMap);
}

bool extract_tile_sets(const json& mapJson, MapRoot* pMap)
{
	if (mapJson.count("tilesets") == 0)
	{
		return true;
	}

	auto tilesetJson = mapJson["tilesets"];

	if (!tilesetJson.is_array())
	{
		spdlog::error("{} Invalid tileset array!", MAP_PARSE_ERR);
		return false;
	}

	const uint64 TILESET_COUNT = tilesetJson.size();

	if (TILESET_COUNT == 0)
	{
		return true;
	}
	pMap->tilesetVector.resize(TILESET_COUNT);

	for (uint64 i = 0; i < TILESET_COUNT; ++i)
	{
		if (!extract_singular_tileset(tilesetJson[i], &pMap->tilesetVector[i]))
		{
			return false;
		}
	}

	return true;
}

bool extract_singular_tileset(const json& tilesetJson, Tileset* pTilesetData)
{
	if (!DOES_ELEMENT_EXIST("tilewidth", tilesetJson))
	{
		spdlog::error("{} No tilewidth field on tileset!", MAP_PARSE_ERR);
		return false;
	}
	GET_NUMBER_INT(tilesetJson, "tilewidth", pTilesetData->tileWidth);

	if (!DOES_ELEMENT_EXIST("tileheight", tilesetJson))
	{
		spdlog::error("{} No tileheight field on tileset!");
		return false;
	}
	GET_NUMBER_INT(tilesetJson, "tileheight", pTilesetData->tileHeight);

	if (!DOES_ELEMENT_EXIST("firstgid", tilesetJson))
	{
		spdlog::error("{} No firstgid field on tileset!", MAP_PARSE_ERR);
		return false;
	}
	GET_NUMBER_INT(tilesetJson, "firstgid", pTilesetData->firstGID);

	if (!DOES_ELEMENT_EXIST("imagewidth", tilesetJson))
	{
		spdlog::error("{} No imagewidth field on tileset!", MAP_PARSE_ERR);
		return false;
	}
	GET_NUMBER_FLOAT(tilesetJson, "imagewidth", pTilesetData->width);

	if (!DOES_ELEMENT_EXIST("imageheight", tilesetJson))
	{
		spdlog::error("{} No imageheight field on tileset!", MAP_PARSE_ERR);
		return false;
	}
	GET_NUMBER_FLOAT(tilesetJson, "imageheight", pTilesetData->height);



	if (tilesetJson.count("name") > 0)
	{
		pTilesetData->name = tilesetJson["name"].get<string>();
	}
	extract_properties_to_map(tilesetJson, pTilesetData->propertiesMap, pTilesetData->propertyTypesMap);
	extract_tile_properties(tilesetJson, pTilesetData->tilePropertiesMap, pTilesetData->tilePropertyTypesMap);
	return true;
}

void extract_tile_properties(const json& jsonObj, std::map<std::string, PropertiesMap>& tilePropMap, std::map<std::string, PropertyTypesMap>& tilePropTypeMap)
{
	auto tile_property_array = jsonObj.find("tiles");
	if (tile_property_array == jsonObj.end())
	{ // not always likely to find properties, so return true
		return;
	}

	//Get total number of properties 
	PropertiesMap temporary_property_map;
	PropertyTypesMap temporary_property_types_map;
	auto tile_property_iterator = tile_property_array->begin();
	while (tile_property_iterator != tile_property_array->end())
	{
		//@UrgentRefactor switch statement to do function calls, and also isolate out the functionality to isolate properties so its generic for other map object types which will have properties.
		const json::value_type& tile_properties_obj = (*tile_property_iterator);
		//@Fix load tile properties for collision data 
		int tileKeyInt = 0;
		GET_NUMBER_INT(tile_properties_obj, "id", tileKeyInt);
		const std::string TILE_KEY = std::to_string(tileKeyInt);

		auto result = tile_properties_obj.find("properties");
		if (tile_properties_obj.find("properties") == tile_properties_obj.end())
		{
			++tile_property_iterator;
			continue;
		}
		const auto beginProp = tile_properties_obj["properties"].begin();
		const auto endProp = tile_properties_obj["properties"].end();

		for (auto i = beginProp; i != endProp; ++i)
		{
			auto tilePropEntry = *i;
			const std::string Type_Name = tilePropEntry["type"].get<string>();
			const PropertyTypes Type_Enum = get_property_type_by_string(Type_Name); //isolate the data type of this property 
			Property mapPropertyUnion{ 0 };
			bool bLoadedCorrectly = true;

			std::string key, value, type;
			key = tilePropEntry["name"].get<string>();
			switch (Type_Enum)
			{
			case PropertyTypes::String:
			{
				value = tilePropEntry["value"].get<string>();
				if (value.size() < MAX_PROPERTY_STRING_CHARS)
				{
					strcpy_s(mapPropertyUnion.type_string, MAX_PROPERTY_STRING_CHARS, value.c_str());
					//propMap->propertyTypes.emplace(key, Type_Enum);
					temporary_property_map.emplace(key, mapPropertyUnion);
				}
				else
				{
					bLoadedCorrectly = false;
					MAP_PARSE_ERR;
					spdlog::debug("{} Unable to load string property from JSON as string is too large!: {}", MAP_PARSE_ERR, value.c_str());
				}
			}
			break;

			case PropertyTypes::Int:
			{
				mapPropertyUnion.type_int = tilePropEntry["value"].get<int>();
				temporary_property_map.emplace(key, mapPropertyUnion);
			}
			break;

			case PropertyTypes::Float:
			{
				mapPropertyUnion.type_float = tilePropEntry["value"].get<float>();
				temporary_property_map.emplace(key, mapPropertyUnion);
			}
			break;

			case PropertyTypes::Bool:
			{
				mapPropertyUnion.type_bool = tilePropEntry["value"].get<bool>();
				temporary_property_map.emplace(key, mapPropertyUnion);
			}
			break;

			case PropertyTypes::HexColour:
			{
				//@Rethink Horrible way of parsing hex value of colours 
				//key = sf::String(properties_iterator.key()).toWideString();
				value = tilePropEntry["value"].get<string>();


				//Colours are represented by tiled as
				//# ALPHA_BITS RED_BITS BLUE_BITS GREEN_BITS
				//Example
				//#ff2d0044 => # FF 2d 00 44 (or 45,0,68,255 as RGBA)
				//
				//To parse this correctly 4 strings contain each channels hex string representation of the value.
				//This is then put through a stringstream to allow conversion directly to int


				std::string alphaChannelChars, redChannelHexChars, greenChannelHexChars, blueChannelHexChars;
				alphaChannelChars.push_back(value[1]);
				alphaChannelChars.push_back(value[2]);

				redChannelHexChars.push_back(value[3]);
				redChannelHexChars.push_back(value[4]);

				greenChannelHexChars.push_back(value[5]);
				greenChannelHexChars.push_back(value[6]);

				blueChannelHexChars.push_back(value[7]);
				blueChannelHexChars.push_back(value[8]);

				int32 AlphaValue, RedValue, BlueValue, GreenValue;
				stringstream ss;
				ss << std::hex << alphaChannelChars;
				ss >> AlphaValue;
				ss.clear();

				ss << std::hex << redChannelHexChars;
				ss >> RedValue;
				ss.clear();

				ss << std::hex << blueChannelHexChars;
				ss >> BlueValue;
				ss.clear();

				ss << std::hex << greenChannelHexChars;
				ss >> GreenValue;
				ss.clear();
				mapPropertyUnion.type_colour = Colour((int8)RedValue, (int8)GreenValue, (int8)BlueValue, (int8)AlphaValue);

				temporary_property_map.emplace(key, mapPropertyUnion);
				break;
			}

			case PropertyTypes::File:
			{
				spdlog::error("{} File property not supported in this parser!", MAP_PARSE_ERR);
			}
			break;

			default:
				bLoadedCorrectly = false;
				spdlog::error("{} Unknown tile property type found in tileset, map failed to load correctly!", MAP_PARSE_ERR);
				break;
			}

			if (bLoadedCorrectly)
			{
				temporary_property_types_map.emplace(key, Type_Enum);
			}
		}

		tilePropMap.emplace(TILE_KEY, temporary_property_map);
		tilePropTypeMap.emplace(TILE_KEY, temporary_property_types_map);
		//clear to be re-used in next loop iteration
		temporary_property_map.clear();
		temporary_property_types_map.clear();
		++tile_property_iterator;
	}
}

void extract_tile_properties(const json& jsonObj, PropertiesMap& propMap, PropertyTypesMap& typeMap)
{
	auto property_obj_it = jsonObj.find("tileproperties");
	auto property_types_it = jsonObj.find("tilepropertytypes");

	if (property_obj_it == jsonObj.end() || property_types_it == jsonObj.end())
	{ // not always likely to find properties, so return true
		return;
	}

	//Get total number of properties 
	const uint32 Property_Count = static_cast<uint32>(property_obj_it->size());
	const uint32 Type_Count = static_cast<uint32>(property_types_it->size());

	//Make sure total number of properties matches number of types
	if (Property_Count != Type_Count)
	{
		spdlog::error("{} Mismatch between number of map properties {} and propertytypes {}", MAP_PARSE_ERR, Property_Count, Type_Count);
		return;
	}

	//@UrgentRefactor switch statement to do function calls, and also isolate out the functionality to isolate properties so its generic for other map object types which will have properties.
	json::value_type properties_obj = *property_obj_it;
	properties_obj = *properties_obj.begin();
	json::value_type propertyTypes_obj = *property_types_it;
	propertyTypes_obj = *propertyTypes_obj.begin();

	json::iterator itProperties = properties_obj.begin(), itPropertyTypes = propertyTypes_obj.begin();

	//iterate through all properties & property-types objects
	while (itProperties != properties_obj.end() && itPropertyTypes != propertyTypes_obj.end())
	{
		const std::string Type_Name = itPropertyTypes.value().get<string>();
		const PropertyTypes Type_Enum = get_property_type_by_string(Type_Name); //isolate the data type of this property 

		Property mapPropertyUnion{ 0 };
		bool bLoadedCorrectly = true;
		std::string key, value, type;
		key = itProperties.key();
		switch (Type_Enum)
		{
		case PropertyTypes::String:
		{
			value = itProperties.value().get<string>();
			if (value.size() < MAX_PROPERTY_STRING_CHARS)
			{
				strcpy_s(mapPropertyUnion.type_string, MAX_PROPERTY_STRING_CHARS, value.c_str());
				//propMap->propertyTypes.emplace(key, Type_Enum);
				propMap.emplace(key, mapPropertyUnion);
			}
			else
			{
				bLoadedCorrectly = false;
				spdlog::error("{} Unable to load string property from JSON as string is too large!: {}", MAP_PARSE_ERR, value);
			}
		}
		break;

		case PropertyTypes::Int:
		{
			mapPropertyUnion.type_int = itProperties.value().get<int>();
			propMap.emplace(key, mapPropertyUnion);
		}
		break;

		case PropertyTypes::Float:
		{
			mapPropertyUnion.type_float = itProperties.value().get<float>();
			propMap.emplace(key, mapPropertyUnion);
		}
		break;

		case PropertyTypes::Bool:
		{
			mapPropertyUnion.type_bool = itProperties.value().get<bool>();
			propMap.emplace(key, mapPropertyUnion);
		}
		break;

		case PropertyTypes::HexColour:
		{
			//@Rethink Horrible way of parsing hex value of colours 
			key = itProperties.key();
			value = itProperties.value().get<string>();

			/*
			Colours are represented by tiled as
			# ALPHA_BITS RED_BITS BLUE_BITS GREEN_BITS
			Example
			#ff2d0044 => # FF 2d 00 44 (or 45,0,68,255 as RGBA)

			To parse this correctly 4 strings contain each channels hex string representation of the value.
			This is then put through a stringstream to allow conversion directly to int
			*/

			std::string alphaChannelChars, redChannelHexChars, greenChannelHexChars, blueChannelHexChars;
			alphaChannelChars.push_back(value[1]);
			alphaChannelChars.push_back(value[2]);

			redChannelHexChars.push_back(value[3]);
			redChannelHexChars.push_back(value[4]);

			greenChannelHexChars.push_back(value[5]);
			greenChannelHexChars.push_back(value[6]);

			blueChannelHexChars.push_back(value[7]);
			blueChannelHexChars.push_back(value[8]);

			int32 AlphaValue, RedValue, BlueValue, GreenValue;
			stringstream ss;
			ss << std::hex << alphaChannelChars;
			ss >> AlphaValue;
			ss.clear();

			ss << std::hex << redChannelHexChars;
			ss >> RedValue;
			ss.clear();

			ss << std::hex << blueChannelHexChars;
			ss >> BlueValue;
			ss.clear();

			ss << std::hex << greenChannelHexChars;
			ss >> GreenValue;
			ss.clear();
			mapPropertyUnion.type_colour = Colour((int8)RedValue, (int8)GreenValue, (int8)BlueValue, (int8)AlphaValue);

			propMap.emplace(key, mapPropertyUnion);
			break;
		}

		case PropertyTypes::File:
		{
			bLoadedCorrectly = false;
			spdlog::error("{} File property not supported in this parser!", MAP_PARSE_ERR);
		}
		break;

		default:
			break;
		}

		if (bLoadedCorrectly)
		{
			typeMap.emplace(key, Type_Enum);
		}
		++itProperties;
		++itPropertyTypes;
	}
	_CrtDbgBreak();
}

bool get_string_if_present(string& value, const string& name, const json& jsonObj)
{
	auto strProperty = jsonObj.count(name);
	if (strProperty == 0)
	{
		return false;
	}

	auto result = jsonObj[name];

	if (!result.is_string())
	{
		return false;
	}
	value = result.get<string>();
	return true;
}
